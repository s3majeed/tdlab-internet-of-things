//
//  AppDelegate.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-08.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "AppDelegate.h"
#import "SensorTagDataServices.h"
#import "Defines.h"
#import "UserData.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    SensorTagDataServices *sensorTagDataServices = [[SensorTagDataServices alloc] init];
    [sensorTagDataServices login];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:sensorWarnings
                                               object:nil];
    return YES;
}

-(void) receiveNotification:(NSNotification*)notification
{
    if ([notification.name isEqualToString:sensorWarnings])
    {
        UserData *userData = [UserData sharedInstance];
        
        NSArray *temperatureWarningArray = [notification.userInfo objectForKey:temperatureWarning];
        NSArray *humidityWarningArray = [notification.userInfo objectForKey:humidityWarning];
        NSArray *batterWarningArray = [notification.userInfo objectForKey:batteryWarning];

        int numberOfWarnings = temperatureWarningArray.count + humidityWarningArray.count + batterWarningArray.count;
        
        [userData setSensorWarnings:notification.userInfo];
        
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
