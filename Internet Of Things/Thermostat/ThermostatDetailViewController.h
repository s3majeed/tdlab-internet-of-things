//
//  ThermostatDetailViewController.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-21.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThermostatDetailViewController : UIViewController
{
    UIButton *selectedButton;
}


@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightSideMenuLauncher;
@property (strong, nonatomic) IBOutlet UINavigationItem *navTitle;

@property (strong, nonatomic) IBOutlet UILabel *temperature;
@property (strong, nonatomic) IBOutlet UILabel *costPerHour;
@property (strong, nonatomic) IBOutlet UILabel *hotTipLabel;
@property (strong, nonatomic) IBOutlet UILabel *summaryLabel;
@property (strong, nonatomic) IBOutlet UILabel *fromDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *toDateLabel;

@property (strong, nonatomic) IBOutlet UIButton *increaseTempButton;
@property (strong, nonatomic) IBOutlet UIButton *decreaseTempButton;
@property (strong, nonatomic) IBOutlet UIButton *fromDate;
@property (strong, nonatomic) IBOutlet UIButton *toDate;
@property (strong, nonatomic) IBOutlet UIButton *doneSelectingDate;

@property (strong, nonatomic) IBOutlet UIImageView *hotTipIcon;
@property (strong, nonatomic) IBOutlet UIImageView *doneBackground;
@property (strong, nonatomic) IBOutlet UIImageView *thermostatGraph;

@property (nonatomic) double temperatureDouble;
@property (nonatomic) double costPerHourDouble;

@property (nonatomic) NSDictionary *thermostatData;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)increaseTemp:(UIButton *)sender;
- (IBAction)decreaseTemp:(UIButton *)sender;
- (IBAction)rightSideMenuClicked:(UIBarButtonItem *)sender;
- (IBAction)displayRightSideMenu:(UISwipeGestureRecognizer *)sender;
- (IBAction)dateSelected:(UIDatePicker *)sender;
- (IBAction)selectDate:(UIButton *)sender;
- (IBAction)finishedDateSelection:(UIButton *)sender;

@end