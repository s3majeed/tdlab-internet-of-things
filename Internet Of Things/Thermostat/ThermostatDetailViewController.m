//
//  ThermostatDetailViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-21.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "ThermostatDetailViewController.h"
#import "AppDelegate.h"
#import "HomeScreenViewController.h"

@interface ThermostatDetailViewController ()

@end

@implementation ThermostatDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    self.fromDateLabel.text = @"Apr 04, 2013";
    self.toDateLabel.text = @"Apr 04, 2013";

    _datePicker.hidden = YES;
    _doneSelectingDate.hidden = YES;
    _doneBackground.hidden = YES;

    self.temperature.text = [NSString stringWithFormat:@"%.1f˚C", self.temperatureDouble];
    self.costPerHour.text = [NSString stringWithFormat:@"$%.2f/hour", self.costPerHourDouble];

    NSArray *reportList = [_thermostatData objectForKey:@"reportList"];
    double totalCost = 0;
    
    for (NSDictionary *dictionary in reportList)
    {
        if ([[dictionary objectForKey:@"thermostatIdentifier"] floatValue] == 123456789012)
        {
            NSArray *meterList = [dictionary objectForKey:@"meterList"];
            
            for (NSDictionary *dictionary2 in meterList)
            {
                NSArray *data = [[dictionary2 objectForKey:@"data"] mutableCopy];
                NSMutableArray *data2 = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < data.count; i++)
                {
                    NSArray *columnData = [data[i] componentsSeparatedByString: @","];
                    
                    NSMutableDictionary *dataDictionary = [[NSMutableDictionary alloc] init];
                    [dataDictionary setObject:columnData[0] forKey:@"date"];
                    [dataDictionary setObject:columnData[1] forKey:@"time"];
                    [dataDictionary setObject:columnData[2] forKey:@"tier"];
                    [dataDictionary setObject:columnData[3] forKey:@"energyConsumption"];
                    [dataDictionary setObject:columnData[4] forKey:@"energyCost"];

                    totalCost = [columnData[4] doubleValue] + totalCost;
                    
                    [data2 addObject:dataDictionary];
                }
            }
        }
    }
    
    self.summaryLabel.text = [NSString stringWithFormat:@"This month, you spent $%.2f", totalCost/100];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        HomeScreenViewController *homeScreenViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
        
//        HomeScreenViewController *homeScreenViewController = [self.navigationController.viewControllers indexOfObject:0];
    }
    
    [super viewWillDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)increaseTemp:(UIButton *)sender
{
    self.temperatureDouble = self.temperatureDouble + 0.1;
    self.temperature.text = [NSString stringWithFormat:@"%.1f˚C", self.temperatureDouble];
    
    self.costPerHourDouble = self.costPerHourDouble + 0.002;
    self.costPerHour.text = [NSString stringWithFormat:@"$%.2f/hour", self.costPerHourDouble];

}

- (IBAction)decreaseTemp:(UIButton *)sender
{
    self.temperatureDouble = self.temperatureDouble - 0.1;
    self.temperature.text = [NSString stringWithFormat:@"%.1f˚C", self.temperatureDouble];
    
    self.costPerHourDouble = self.costPerHourDouble - 0.002;
    
    if(_costPerHour < 0)
    {
        _costPerHour = 0;
    }
    
    self.costPerHour.text = [NSString stringWithFormat:@"$%.2f/hour", self.costPerHourDouble];
}

- (IBAction)rightSideMenuClicked:(UIBarButtonItem *)sender
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.rootViewController presentRightMenuViewController];
}

- (IBAction)displayRightSideMenu:(UISwipeGestureRecognizer *)sender
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.rootViewController presentRightMenuViewController];
}

- (IBAction)dateSelected:(UIDatePicker *)sender
{
    if (selectedButton == _fromDate)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM dd, yyyy"];
        NSString *strDate = [dateFormatter stringFromDate:_datePicker.date];
        self.fromDateLabel.text = strDate;
    }
    
    if (selectedButton == _toDate)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM dd, yyyy"];
        NSString *strDate = [dateFormatter stringFromDate:_datePicker.date];
        self.toDateLabel.text = strDate;
    }
}

- (IBAction)selectDate:(UIButton *)sender
{
    selectedButton = sender;
    
    _datePicker.hidden = NO;
    _doneSelectingDate.hidden = NO;
    _doneBackground.hidden = NO;
    _thermostatGraph.hidden = YES;
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:2013];
    [components setMonth:4];
    [components setDay:4];
    NSDate *defualtDate = [calendar dateFromComponents:components];
    _datePicker.date = defualtDate;
    

}

- (IBAction)finishedDateSelection:(UIButton *)sender
{
    _datePicker.hidden = YES;
    _doneSelectingDate.hidden = YES;
    _doneBackground.hidden = YES;
    _thermostatGraph.hidden = NO;

}


@end