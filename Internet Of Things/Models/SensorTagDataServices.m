//
//  SensorTagDataServices.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-27.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "SensorTagDataServices.h"
#import "UserData.h"
#import "ASyncTask.h"
#import "Defines.h"

@implementation SensorTagDataServices

- (void)login
{
    ASyncTask *login = [[ASyncTask alloc] init];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"greeninnovationlab@gmail.com" forKey:@"email"];
    [parameters setObject:@"greenwall" forKey:@"password"];
    
    login.delegate = self;

    [login postRequestWithBaseURL:sensorTagBaseUrl urlPath:signInUrl parameters:parameters];
}

- (void)refreshSensorData
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];

    ASyncTask *tagList = [[ASyncTask alloc] init];
    tagList.delegate = self;
    [tagList postRequestWithBaseURL:sensorTagBaseUrl urlPath:getTagListUrl parameters:parameters];
}

- (void)responseResult:(NSDictionary *)response JSON:(id)json type:(NSString *)type
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if ([[response objectForKey:@"d"] isKindOfClass:[NSNull class]])
    {
        NSLog(@"Get Tag Managers");
        
        ASyncTask *humidityData = [[ASyncTask alloc] init];
        humidityData.delegate = self;
        
        [humidityData postRequestWithBaseURL:sensorTagBaseUrl urlPath:getTagManagersUrl parameters:parameters];
    }
    else if ([[response objectForKey:@"d"] isKindOfClass:[NSArray class]])
    {
        NSArray *data = [response objectForKey:@"d"];
        NSDictionary *tagManager1 = data[0];
        
        if ([[tagManager1 objectForKey:@"__type"]  isEqual: @"MyTagList.ethAccount+TagManagerEntry"])
        {
            NSLog(@"Select Tag Manager");

            NSString *macAddress = [tagManager1 objectForKey:@"mac"];
            
            ASyncTask *humidityData = [[ASyncTask alloc] init];
            humidityData.delegate = self;
            
            [parameters setValue:macAddress forKey:@"mac"];
            
            [humidityData postRequestWithBaseURL:sensorTagBaseUrl urlPath:selectTagManagerURL parameters:parameters];
        }
        else if ([[tagManager1 objectForKey:@"__type"]  isEqual: @"MyTagList.Tag"])
        {
            NSLog(@"Updated Sensor Data");

            UserData *userData = [UserData sharedInstance];
            [userData setSensorData:[response objectForKey:@"d"]];
            
            NSMutableArray *humidityWarningArray = [[NSMutableArray alloc] init];
            NSMutableArray *temperatureWarningArray = [[NSMutableArray alloc] init];
            NSMutableArray *batteryWarningArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *sensorData in [userData getSensorData])
            {
                double humidityLevel = [[sensorData objectForKey:@"cap"] doubleValue];
                double temperatureLevel = [[sensorData objectForKey:@"temperature"] doubleValue];
                double batteryLevel = [[sensorData objectForKey:@"batteryRemaining"] doubleValue] * 100;
                
                if (humidityLevel > 70)
                {
                    [humidityWarningArray addObject:sensorData];
                }
                
                if (temperatureLevel > 25.8 || temperatureLevel < 10)
                {
                    [temperatureWarningArray addObject:sensorData];
                }
                
                if (batteryLevel < 10)
                {
                    [batteryWarningArray addObject:sensorData];
                }
            }
            
            NSMutableDictionary *warnings = [[NSMutableDictionary alloc] init];
            [warnings setObject:humidityWarningArray forKey:humidityWarning];
            [warnings setObject:temperatureWarningArray forKey:temperatureWarning];
            [warnings setObject:batteryWarningArray forKey:batteryWarning];

            [[NSNotificationCenter defaultCenter]
             postNotificationName:sensorWarnings
             object:self
             userInfo:warnings];
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:updatedSensorData
             object:self
             userInfo:response];
            
            if (refreshSensorDataTimer == nil)
            {
                refreshSensorDataTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                 target:self
                                               selector:@selector(refreshSensorData)
                                               userInfo:nil
                                                repeats:YES];
            }
        }
    }
    else if ([[response objectForKey:@"d"] isKindOfClass:[NSNumber class]])
    {
        NSLog(@"Get Tag List");

        ASyncTask *tagList = [[ASyncTask alloc] init];
        tagList.delegate = self;
        
        [tagList postRequestWithBaseURL:sensorTagBaseUrl urlPath:getTagListUrl parameters:parameters];
    }
}

- (void)responseFailure:(NSError *)error type:(NSString *)type
{
    NSLog(@"Error: %@", error);
    
}

@end
