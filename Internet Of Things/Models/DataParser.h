//
//  DataParser.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-08.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataParser : NSObject
{
    
}

+ (DataParser *)sharedInstance;

- (BOOL)parseLoginResponse:(id)response;
- (NSMutableDictionary *)parseAccountsResponse:(id)response;

@end
