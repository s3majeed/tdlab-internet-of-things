//
//  DataParser.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-08.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "DataParser.h"
#import "UserData.h"

@implementation DataParser

+ (DataParser *)sharedInstance
{
    static DataParser *sharedUserData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUserData = [[self alloc] init];
    });
    return sharedUserData;
}

- (BOOL)parseLoginResponse:(id)response
{
    UserData *user = [UserData sharedInstance];

    if ([response isKindOfClass:[NSDictionary class]])
    {
        int li = [[response valueForKey:@"isLoggedIn"] intValue];
        
        if (li > 0)
        {
            [user setLoggedIn:YES];
            
            user.mfaOn = ([[response valueForKey:@"isMFAOn"] intValue] > 0);
            user.eligibleTradingAccounts = ([[response valueForKey:@"hasEligibleStockTradingAccounts"] intValue] > 0);
            user.omnitureOn = ([[response valueForKey:@"isOmnitureOn"] intValue] > 0);
            
            user.jSessionId = [response valueForKey:@"jSessionId"];
            user.masterAccount = [response valueForKey:@"masterAcct"];
            user.sessionType = [response valueForKey:@"sessionType"];
            
            return YES;
        }
        else
        {
            NSDictionary *loginBean = (NSDictionary *)[response objectForKey:@"loginMenuBean"];
            
            NSString *errorCode = (NSString *)[loginBean objectForKey:@"errorCode"];
            NSString *message = (NSString *)[loginBean objectForKey:@"message"];
            user.errorCode = [NSString stringWithFormat:@"%@ %@", message, errorCode];
        }
    }
    else
    {
        user.errorCode = [NSString stringWithFormat:@"Could not connect to server"];
    }
    
    return NO;
}

- (NSMutableDictionary *)parseAccountsResponse:(id)response
{
    UserData *user = [UserData sharedInstance];
    user.accountEntries = [[NSMutableDictionary alloc] init];
    
    NSLog(@"%@", response);
    
    NSDictionary *bean = [response objectForKey:@"bean"];
    
    NSMutableArray *accounts = [[bean objectForKey:@"accounts"] mutableCopy];
    for (NSDictionary *acct in accounts)
    {
        [user addAccountEntry:acct];
    }
    
    NSMutableArray *sortedAccountKeys = [[[user.accountEntries allKeys] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
    user.sortedAccountKeys = sortedAccountKeys;

    return user.accountEntries;
}

@end
