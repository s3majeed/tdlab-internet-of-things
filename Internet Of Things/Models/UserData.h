//
//  UserData.h
//  TDMobile
//
//  Created by Scott Abdey on 13-07-03.
//  Amended by Sulaiman Majeed on 13-10-19.
//
//  Copyright (c) 2013 TDBFG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserData : NSObject
{
    NSArray *sensorData;
    NSDictionary *sensorWarnings;
}

+ (UserData *)sharedInstance;

// TD Authentication
@property (nonatomic, assign, getter = isLoggedIn) BOOL loggedIn;
@property (nonatomic, assign, getter = isMFAOn) BOOL mfaOn;
@property (nonatomic, assign, getter = isOmnitureON) BOOL omnitureOn;
@property (nonatomic, assign, getter = isTradeEligible) BOOL eligibleTradingAccounts;

@property (strong, nonatomic) NSString *jSessionId;
@property (strong, nonatomic) NSString *masterAccount;
@property (strong, nonatomic) NSString *sessionType;
@property (strong, nonatomic) NSString *confirmation;

- (void)deleteUserCookies;

// Accounts
@property (nonatomic, retain) NSMutableDictionary *accountEntries;
@property (nonatomic, retain) NSArray *sortedAccountKeys;

// Thermostat Data
@property (nonatomic, retain) NSDictionary *thermostatData;

- (NSDictionary *)accountAtIndex:(int)row section:(int)section;
- (BOOL)parseAccountDetailResponse:(id)response;
- (int)accountSections;
- (int)accountsInSection:(int)section;
- (void)addAccountEntry:(NSDictionary *)account;

// Server Error
@property (strong, nonatomic) NSString *errorCode;

// Sensor Data
- (NSArray *)getSensorData;
- (void)setSensorData:(NSArray *)newSensorData;

- (NSDictionary *)getSensorWarnings;
- (void)setSensorWarnings:(NSDictionary *)newSensorWarnings;

// Thermostat Data
- (NSDictionary *)getThermostatData;
- (void)setThermostatData:(NSDictionary *)newThermostatData;


// Transfer Funds
- (BOOL)transferFromAccountList:(id)response;
- (BOOL)transferToAccountList:(id)response;
- (BOOL)transactionNumber:(id)response;






- (BOOL)confirmation:(id)response;



@end
