//
//  ASyncTask.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-10.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "ASyncTask.h"
#import "AFNetworking.h"

@implementation ASyncTask

- (void)postRequestWithBaseURL:(NSString *)baseURL urlPath:(NSString *)urlPath parameters:(NSDictionary *)parameters;
{
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:urlPath
                                                      parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    NSLog(@"Request: %@", request);
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         // Print the response body in text
         NSLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
         
         NSData *data = [[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
         NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:kNilOptions
                                                                        error:nil];
         
         NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         [_delegate responseResult:jsonResponse JSON:jsonString type:_type];
         
         //         NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding], nil];
         //
         //         NSLog(@"Test: %@", dictionary);
         
     }
    
     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [_delegate responseFailure:error type:self.type];
     }];
    
    [operation start];
}

- (void)getWithURLRequest:(NSURLRequest *)request type:(NSString *)type
{
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    {
        NSLog(@"JSON: %@", JSON);
        
        if ([JSON isKindOfClass:[NSDictionary class]])
        {
            [_delegate responseResult:JSON JSON:JSON type:type];
        }
        else
        {
            NSString *jsonString = [[NSString alloc] initWithData:JSON encoding:NSUTF8StringEncoding];
            
            NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:kNilOptions
                                                                           error:nil];
            
            [_delegate responseResult:jsonResponse JSON:jsonString type:type];
        }
    }

    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
    {
        [_delegate responseFailure:error type:type];
    }];
    
    [operation start];
}

- (void)getWithURLString:(NSString *)urlString type:(NSString *)type
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
     {
         NSLog(@"JSON: %@", JSON);
         
         if ([JSON isKindOfClass:[NSDictionary class]])
         {
             [_delegate responseResult:JSON JSON:JSON type:type];
         }
         else
         {
             NSString *jsonString = [[NSString alloc] initWithData:JSON encoding:NSUTF8StringEncoding];
             
             NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
             
             NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:kNilOptions
                                                                            error:nil];
             [_delegate responseResult:jsonResponse JSON:jsonString type:type];
         }
     }
     
    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
     {
         [_delegate responseFailure:error type:type];
     }];
    
    [operation start];
}



@end
