//
//  ASyncTask.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-10.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ASyncTaskDelegate <NSObject>

@required

- (void)responseResult:(NSDictionary *)response JSON:(id)json type:(NSString *)type;
- (void)responseFailure:(NSError *)error type:(NSString *)type;

@end

@interface ASyncTask : NSObject
{
    
}

@property (nonatomic, weak) NSString *type;
@property (nonatomic, retain) id delegate;

- (void)postRequestWithBaseURL:(NSString *)baseURL urlPath:(NSString *)urlPath parameters:(NSDictionary *)parameters;
- (void)getWithURLRequest:(NSURLRequest *)request type:(NSString *)type;
- (void)getWithURLString:(NSString *)urlString type:(NSString *)type;

@end
