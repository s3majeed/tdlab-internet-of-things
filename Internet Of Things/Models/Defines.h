//
//  Defines.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-28.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#ifndef Internet_Of_Things_Defines_h
#define Internet_Of_Things_Defines_h

#pragma mark Wireless Tag API

#define sensorTagBaseUrl    @"http://mytaglist.com"

#define beepUrl             @"/ethClient.asmx/Beep"
#define stopBeepUrl         @"/ethClient.asmx/StopBeep"
#define getTagManagersUrl   @"/ethAccount.asmx/GetTagManagers"
#define selectTagManagerURL @"/ethAccount.asmx/SelectTagManager"
#define getTagListUrl       @"/ethClient.asmx/GetTagList"
#define signInUrl           @"/ethAccount.asmx/SignIn"


#pragma mark NSNotification Names

#define updatedSensorData       @"sensorDataUpdated"

#define humidityWarning         @"humidityWarning"
#define batteryWarning          @"batteryWarning"
#define temperatureWarning      @"temperatureWarning"
#define sensorWarnings          @"sensorWarnings"



#endif
