//
//  UserData.m
//  TDMobile
//
//  Created by Scott Abdey on 13-07-03.
//  Amended by Sulaiman Majeed on 13-10-19.
//
//  Copyright (c) 2013 TDBFG. All rights reserved.
//

#import "UserData.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@implementation UserData

NSDictionary *accountGroupNames;

+ (UserData *)sharedInstance
{
    static UserData *sharedUserData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUserData = [[self alloc] init];
    });
    return sharedUserData;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"AccountGroups" ofType:@"plist"];
        
        accountGroupNames = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    }
    
    return self;
}

- (void)addAccountEntry:(NSDictionary *)account
{
    NSString *title = [NSString stringWithFormat: @"%@ - %@ - %@",
                       [account objectForKey:@"description"],
                       [account objectForKey:@"acctNumber"],
                       [account objectForKey:@"acct"]];
    
    NSString *balance = [account objectForKey:@"balance"];
    NSString *group = [account objectForKey:@"group"];
    NSString *description = [account objectForKey:@"description"];
    NSString *acct = [account objectForKey:@"acct"];
    NSString *acctNumber = [account objectForKey:@"acctNumber"];

    NSString *section = [accountGroupNames objectForKey:group];
    
    // NSLog(@"section: %@", section);
    
    NSMutableDictionary *entry = [[NSMutableDictionary alloc] init];
    [entry setObject:title forKey:@"title"];
    [entry setObject:balance forKey:@"balance"];
    [entry setObject:group forKey:@"group"];
    [entry setObject:description forKey:@"description"];
    
    if (acct != nil)
    {
        [entry setObject:acct forKey:@"acct"];
    }

    [entry setObject:acctNumber forKey:@"acctNumber"];

    NSMutableArray *sectionEntries = [self.accountEntries objectForKey:section];
    
    if (sectionEntries == nil)
    {
        sectionEntries = [[NSMutableArray alloc] init];
        
        [self.accountEntries setObject:sectionEntries forKey:section];
    }
    
    [sectionEntries addObject:entry];
    // NSLog(@"Entry: %@", entry);
    
    //NSLog(@"Accounts: %d", accountEntries.count);
}

- (int)accountSections
{
    return _accountEntries.count;
}

- (int)accountsInSection:(int)section
{
    NSString *sectionName = [self accountSectionName:section];
    
    NSMutableArray *sectionEntries = [_accountEntries objectForKey:sectionName];
    return sectionEntries.count;
}

- (NSString *)accountSectionName:(int)section
{
    NSMutableArray *sortedAccountKeys = [[[_accountEntries allKeys] sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
    return [sortedAccountKeys objectAtIndex:section];
}

- (NSDictionary *)accountAtIndex:(int)row section:(int)section
{
    NSString *sectionName = [self accountSectionName:section];
    NSMutableArray *sectionEntries = [_accountEntries objectForKey:sectionName];
    return [sectionEntries objectAtIndex:row];
}

- (NSArray *)getSensorData
{
    return sensorData;
}

- (void)setSensorData:(NSArray *)newSensorData
{
    sensorData = newSensorData;
}

- (void)deleteUserCookies
{
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (cookie in [storage cookies])
    {
        // NSLog(@"Cookie: %@", cookie);
        [storage deleteCookie:cookie];
        
    }
    
    [self setLoggedIn:NO];
}


- (NSDictionary *)getThermostatData
{
    return self.thermostatData;
}

- (void)setThermostatData:(NSDictionary *)newThermostatData
{
    self.thermostatData = newThermostatData;
}

- (NSDictionary *)getSensorWarnings
{
    return sensorWarnings;
}

- (void)setSensorWarnings:(NSDictionary *)newSensorWarnings
{
    sensorWarnings = newSensorWarnings;
}


//- (BOOL)parseAccountDetailResponse:(id)response
//{
//    // NSLog(@"%@", response);
//    
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    appDelegate.creditCardData = [[NSDictionary alloc] init];
//    
//    NSDictionary *bean = [response objectForKey:@"bean"];
//    
//    appDelegate.accountActivityError = [bean objectForKey:@"message"];
//
//    if ([appDelegate.sectionSelected  isEqual: @"Credit"])
//    {
//        appDelegate.creditCardData = bean;
//        // NSLog(@"Credit Card Data: %@", appDelegate.creditCardData);
//        
//    }
//    
//    appDelegate.accountDetails = [[NSMutableArray alloc] init];
//    
//    [appDelegate.accountDetails removeAllObjects];
//    
//    NSArray *accountDetail = [bean objectForKey:@"log"];
//    appDelegate.accountData = bean;
//
//    for (NSDictionary *acctDetail in accountDetail)
//    {
//        [self addAccountDetail:acctDetail];
//        // NSLog(@"Account Detail Added: %@", acctDetail);
//    }
//    
//    //sortedAccountDetails = [[accountDetails allKeys] sortedArrayUsingSelector:@selector(compare:)];
//    
//    return YES;
//    
//    //return NO;
//}

//- (BOOL)confirmation:(id)response
//{
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    NSDictionary *bean = [response objectForKey:@"bean"];
//    
//    _confirmation = [bean objectForKey:@"confirmation"];
//    appDelegate.transactionErrorMessage = [bean objectForKey:@"message"];
//
//    return YES;
//}

//- (void)addAccountDetail:(NSDictionary *)acctDetail
//{
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//
//    NSString *amount = [acctDetail objectForKey:@"amount"];
//    NSString *description = [acctDetail objectForKey:@"description"];
//    NSString *date = [acctDetail objectForKey:@"date"];
//    
//    // NSLog(@"section: %@", section);
//    
//    NSMutableDictionary *entry = [[NSMutableDictionary alloc] init];
//    [entry setObject:amount forKey:@"amount"];
//    [entry setObject:description forKey:@"description"];
//    [entry setObject:date forKey:@"date"];
//    
//    [appDelegate.accountDetails addObject:entry];
//    
//    //NSLog(@"Account Details: %@", appDelegate.accountDetails);
//}


//- (BOOL)transferFromAccountList:(id)response
//{
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    NSDictionary *bean = [response objectForKey:@"bean"];
//    appDelegate.transferFromArray = [bean objectForKey:@"accounts"];
//
//    if ([bean objectForKey:@"message"] == nil)
//    {
//        appDelegate.transferFromErrorMessage = @"N/A";
//    }
//    else
//    {
//        appDelegate.transferFromErrorMessage = [bean objectForKey:@"message"];
//    }
//    
//    return YES;
//}
//
//- (BOOL)transferToAccountList:(id)response
//{
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    NSDictionary *bean = [response objectForKey:@"bean"];
//    appDelegate.transferToArray = [bean objectForKey:@"accounts"];
//    
//    if ([bean objectForKey:@"message"] == nil)
//    {
//        appDelegate.transferToErrorMessage = @"N/A";
//    }
//    else
//    {
//        appDelegate.transferToErrorMessage = [bean objectForKey:@"message"];
//    }
//
//    return YES;
//}

//- (BOOL)transactionNumber:(id)response
//{
//    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
//    
//    NSDictionary *bean = [response objectForKey:@"bean"];
//    appDelegate.transactionNumber = [bean objectForKey:@"transaction"];
//    appDelegate.transactionNumber = [bean objectForKey:@"message"];
//
//    return YES;
//}


@end
