//
//  SensorTagDataServices.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-27.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASyncTask.h"

@protocol SensorTagDataDelegate <NSObject>

@optional

- (void)setSensorTagData:(NSArray *)data;

@end

@interface SensorTagDataServices : NSObject <ASyncTaskDelegate>
{
    NSTimer *refreshSensorDataTimer;
}

@property (nonatomic, weak) id delegate;

- (void)login;

@end
