//
//  WirelessConnection.m
//  TDMobile
//
//  Created by Scott Abdey on 2013-07-04.
//
//  Copyright (c) 2013 TDBFG. All rights reserved.
//

#import "WirelessConnection.h"
#import "UserData.h"

@implementation WirelessConnection

NSDictionary *settings;

NSString* const authenticationServlet = @"servlet/ca.tdbank.wireless3.servlet.AuthenticateJsonServlet";
NSString* const bankingServlet = @"servlet/ca.tdbank.wireless3.servlet.BankingJsonServlet";


+ (WirelessConnection *)sharedInstance
{
    static WirelessConnection *sharedConnectionData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedConnectionData = [[self alloc] init];
    });
    return sharedConnectionData;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Wireless" ofType:@"plist"];
        
        settings = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    }
    
    return self;
}

- (NSURLRequest *)getAuthenticationRequest:(NSDictionary *)params
{
    return [self getRequestString:params forServlet:authenticationServlet];
}

- (NSURLRequest *)getBankingRequest:(NSDictionary *)params
{
    return [self getRequestString:params forServlet:bankingServlet];
}

- (NSURLRequest *)getRequestString:(NSDictionary *)params forServlet:(NSString *)servlet
{
    NSMutableArray *queryParameters = [[NSMutableArray alloc] init];
    
    NSDictionary *configParameters = [settings objectForKey:@"parameters"];
    for (id key in configParameters)
    {
        id value = [configParameters objectForKey: key];
        NSString *item = [NSString stringWithFormat: @"%@=%@", key, value];
        [queryParameters addObject:item];
    }
    
    [queryParameters addObject:@"LOGONTO=BANKE"];   // Handle language
    
    for (id key in params)
    {
        id value = [params objectForKey: key];
        NSString *item = [NSString stringWithFormat: @"%@=%@", key, value];
        [queryParameters addObject:item];
    }
    
    NSString *query = [NSString stringWithFormat:@"%@/%@?%@",
                       [settings objectForKey: @"server"],
                       servlet,
                       [queryParameters componentsJoinedByString:@"&"]];
    
    //NSLog(@"Query: %@", query);
    
    NSURL *url = [NSURL URLWithString:query];
    NSLog(@"URL: %@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    return request;
}

- (NSURLRequest *)getBranchLocatorRequest:(NSDictionary *)params
{
    NSMutableArray *queryParameters = [[NSMutableArray alloc] init];

    for (id key in params)
    {
        id value = [params objectForKey: key];
        NSString *item = [NSString stringWithFormat: @"%@=%@", key, value];
        [queryParameters addObject:item];
    }
    
    NSString *query = [NSString stringWithFormat:@"http://www.tdbank.com/net/get6.ashx?%@",
                       [queryParameters componentsJoinedByString:@"&"]];
    
    NSLog(@"Query: %@", query);
    
    NSURL *url = [NSURL URLWithString:query];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    return request;
}

- (void)logout
{
    UserData *user = [UserData sharedInstance];
    [user setLoggedIn:NO];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (cookie in [storage cookies])
    {
        NSLog(@"Cookie: %@", cookie);
        [storage deleteCookie:cookie];
        
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
