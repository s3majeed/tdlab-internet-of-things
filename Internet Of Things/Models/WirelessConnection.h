//
//  WirelessConnection.h
//  TDMobile
//
//  Created by Scott Abdey on 2013-07-04.
//
//  Copyright (c) 2013 TDBFG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WirelessConnection : NSObject

+ (WirelessConnection *)sharedInstance;

- (NSURLRequest *)getAuthenticationRequest:(NSDictionary *)params;
- (NSURLRequest *)getBankingRequest:(NSDictionary *)params;
- (NSURLRequest *)getBranchLocatorRequest:(NSDictionary *)params;

- (void)logout;

@end
