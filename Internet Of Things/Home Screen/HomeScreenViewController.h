//
//  HomeScreenViewController.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-14.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeScreenViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *thermostatContainer;
@property (strong, nonatomic) IBOutlet UIView *sensorContainer;

@end
