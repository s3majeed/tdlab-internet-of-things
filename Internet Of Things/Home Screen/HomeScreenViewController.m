//
//  HomeScreenViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-14.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "HomeScreenViewController.h"
#import "AppDelegate.h"

@interface HomeScreenViewController ()

@end

@implementation HomeScreenViewController

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    
    
//    appDelegate.navBarHeight = 150;
//    appDelegate.defaultNavBackground = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Home Backdrop.png"] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillDisappear:(BOOL)animated
{
//    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
//    appDelegate.navBarHeight = 44;
//    [self.navigationController.navigationBar setBackgroundImage:appDelegate.defaultNavBackground forBarMetrics:UIBarMetricsDefault];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
