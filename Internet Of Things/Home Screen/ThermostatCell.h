//
//  ThermostatCell.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-16.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThermostatCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UIImageView *thermostatIcon;
@property (strong, nonatomic) IBOutlet UILabel *savingsLabel;

@property (strong, nonatomic) IBOutlet UILabel *sensor;

@end
