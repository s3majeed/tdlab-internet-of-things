//
//  ThermostatTableViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-16.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "ThermostatTableViewController.h"
#import "ThermostatCell.h"
#import "HumidityTableViewController.h"
#import "HumidityCell.h"
#import "ASyncTask.h"
#import "SensorTagDataServices.h"
#import "UserData.h"
#import "HumidityDetailViewController.h"
#import "Defines.h"

@interface HumidityTableViewController ()

@end

@implementation HumidityTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.transform=CGAffineTransformMakeRotation(-M_PI_2);

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                             name:updatedSensorData
                                             object:nil];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;    
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) receiveNotification:(NSNotification*)notification
{
    if ([notification.name isEqualToString:updatedSensorData])
    {
        [self.tableView reloadData];
    }
}

- (void)responseResult:(NSDictionary *)response JSON:(id)json type:(NSString *)type
{
    
}

- (void)responseFailure:(NSError *)error type:(NSString *)type
{
    NSLog(@"Error: %@", error);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    UserData *userData = [UserData sharedInstance];
    return [userData getSensorData].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HumidityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HumidityCell" forIndexPath:indexPath];
    
    UserData *userData = [UserData sharedInstance];
    NSDictionary *data = [userData getSensorData][indexPath.row];
    
    cell.temperatureLabel.text = [NSString stringWithFormat:@"Humidity: %.1f%%", [[data objectForKey:@"cap"] doubleValue]];
    cell.locationLabel.text = [NSString stringWithFormat:@"%@", [data objectForKey:@"name"]];
    cell.moistureLabel.text = [NSString stringWithFormat:@"Temperature: %.1f˚C", [[data objectForKey:@"temperature"] doubleValue]];

    cell.contentView.transform=CGAffineTransformMakeRotation(M_PI_2);

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UserData *userData = [UserData sharedInstance];
    
    // Present the thermostat view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HumidityDetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"HumidityDetails"];
    vc.sensorData = [userData getSensorData][indexPath.row];
    vc.navTitle.title = [vc.sensorData objectForKey:@"name"];

    [self.navigationController pushViewController:vc animated:YES];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
