//
//  ThermostatTableViewController.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-16.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASyncTask.h"
#import "SensorTagDataServices.h"

@interface HumidityTableViewController : UITableViewController <ASyncTaskDelegate, SensorTagDataDelegate>

@end
