//
//  ThermostatTableViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-16.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "ThermostatTableViewController.h"
#import "ThermostatCell.h"
#import "ThermostatDetailViewController.h"
#import "AsyncTask.h"

@interface ThermostatTableViewController ()

@end

@implementation ThermostatTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.transform=CGAffineTransformMakeRotation(-M_PI_2);

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ThermostatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ThermostatCell" forIndexPath:indexPath];

    if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1)
    {
        cell.locationLabel.text = @"Add";
        cell.temperatureLabel.hidden = YES;
        cell.savingsLabel.hidden = YES;
        cell.thermostatIcon.image = [UIImage imageNamed:@"Add Thermostat.png"];
        
        cell.contentView.transform=CGAffineTransformMakeRotation(M_PI_2);
    }
    else
    {
        cell.temperatureLabel.text = @"23.0˚C";
        cell.locationLabel.text = @"Living room";
        cell.savingsLabel.text = @"$0.40/hour";
        
        cell.contentView.transform=CGAffineTransformMakeRotation(M_PI_2);
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if(indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1)
    {
        
    }
    else
    {
        ASyncTask *thermostatData = [[ASyncTask alloc] init];
        thermostatData.delegate = self;
        
        NSDate *currentDate = [[NSDate alloc] init];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setObject:@"2010-01-01" forKey:@"startDate"];
        [parameters setObject:@"2010-01-02" forKey:@"endDate"];
        [parameters setObject:@"energy" forKey:@"meters"];
        
        NSMutableDictionary *selectionParameters = [[NSMutableDictionary alloc] init];
        [selectionParameters setObject:@"thermostats" forKey:@"selectionType"];
        [selectionParameters setObject:@"123456789012" forKey:@"selectionMatch"];

        [parameters setObject:selectionParameters forKey:@"selection"];

//        [thermostatData getWithURLString:[NSString stringWithFormat:@"https://api.ecobee.com/1/meterReport?format=json&body=%@", parameters] type:@"thermostatData"];

        [thermostatData getWithURLString:@"https://api.myjson.com/bins/240c5" type:@"thermostatData"];
    }
}


- (void)responseResult:(NSDictionary *)response JSON:(id)json type:(NSString *)type
{
    NSLog(@"Dictionary: %@", response);
    NSLog(@"JSON: %@", json);
    
    if ([type  isEqual: @"thermostatData"])
    {
        // Present the thermostat view controller
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ThermostatDetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ThermostatDetails"];
        vc.navTitle.title = @"Living Room";
        vc.temperatureDouble = 23.0;
        vc.costPerHourDouble = 0.40;
        vc.thermostatData = response;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
