//
//  NavigationViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-14.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "NavigationViewController.h"
#import "AppDelegate.h"

@interface UINavigationBar (myNave)
- (CGSize)changeHeight:(CGSize)size;
@end

@implementation UINavigationBar (customNav)

- (CGSize)sizeThatFits:(CGSize)size
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    CGSize newSize = CGSizeMake(appDelegate.window.bounds.size.width, appDelegate.navBarHeight);
    NSLog(@"Width: %f    Height: %d   ", appDelegate.window.bounds.size.width, appDelegate.navBarHeight);
    return newSize;
}

@end

@implementation NavigationViewController

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
