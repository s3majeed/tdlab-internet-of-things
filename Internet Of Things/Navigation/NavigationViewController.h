//
//  NavigationViewController.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-14.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationViewController : UINavigationController

@end
