//
//  HumidityDetailViewController.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-27.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASyncTask.h"

@interface HumidityDetailViewController : UIViewController <ASyncTaskDelegate>

@property (strong, nonatomic) IBOutlet UILabel *sensorBatteryLevel;
@property (strong, nonatomic) IBOutlet UILabel *humidityLevel;
@property (strong, nonatomic) IBOutlet UILabel *temperatureLevel;
@property (strong, nonatomic) NSDictionary *sensorData;

@property (strong, nonatomic) IBOutlet UINavigationItem *navTitle;

- (IBAction)displayRightSideMenu:(UISwipeGestureRecognizer *)sender;
- (IBAction)beep:(UIButton *)sender;
- (IBAction)stopBeep:(UIButton *)sender;
- (IBAction)rightMenuSelected:(UIBarButtonItem *)sender;

@end
