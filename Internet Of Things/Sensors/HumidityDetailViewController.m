//
//  HumidityDetailViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-27.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "HumidityDetailViewController.h"
#import "AppDelegate.h"
#import "AsyncTask.h"
#import "Defines.h"
#import "HomeScreenViewController.h"

@interface HumidityDetailViewController ()

@end

@implementation HumidityDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sensorBatteryLevel.text = [NSString stringWithFormat:@"%.0f%% ", [[_sensorData objectForKey:@"batteryRemaining"] doubleValue] * 100];
    self.humidityLevel.text = [NSString stringWithFormat:@"Humidity: %.1f%%", [[self.sensorData objectForKey:@"cap"] doubleValue]];
    self.temperatureLevel.text = [NSString stringWithFormat:@"Temperature: %.1f˚C", [[self.sensorData objectForKey:@"temperature"] doubleValue]];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:updatedSensorData
                                               object:nil];
    
//    if ([self.sensorData objectForKey:@"temperature"])
//    {
//        
//    }
    
    // Do any additional setup after loading the view.
}

-(void) receiveNotification:(NSNotification*)notification
{
    if ([notification.name isEqualToString:updatedSensorData])
    {
        NSDictionary *userInfo = notification.userInfo;
        NSArray *tagList = [userInfo objectForKey:@"d"];
        
        int slaveID = [[_sensorData objectForKey:@"slaveId"] intValue];

        for (NSDictionary *tagData in tagList)
        {
            if ([[tagData objectForKey:@"slaveId"] intValue] == slaveID)
            {
                self.sensorData = tagData;
                break;
            }
        }
        
        self.sensorBatteryLevel.text = [NSString stringWithFormat:@"%.0f%% ", [[self.sensorData objectForKey:@"batteryRemaining"] doubleValue] * 100];
        self.humidityLevel.text = [NSString stringWithFormat:@"Humidity: %.1f%%", [[self.sensorData objectForKey:@"cap"] doubleValue]];
        self.temperatureLevel.text = [NSString stringWithFormat:@"Temperature: %.1f˚C", [[self.sensorData objectForKey:@"temperature"] doubleValue]];
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        HomeScreenViewController *homeScreenViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
        
        //        HomeScreenViewController *homeScreenViewController = [self.navigationController.viewControllers indexOfObject:0];
    }
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender 
 {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)displayRightSideMenu:(UISwipeGestureRecognizer *)sender
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.rootViewController presentRightMenuViewController];
}

- (IBAction)beep:(UIButton *)sender
{
    ASyncTask *beep = [[ASyncTask alloc] init];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"2" forKey:@"id"];
    [parameters setObject:@"500" forKey:@"beepDuration"];
    
    beep.delegate = self;
    [beep postRequestWithBaseURL:sensorTagBaseUrl urlPath:beepUrl parameters:parameters];
}

- (IBAction)stopBeep:(UIButton *)sender
{
    ASyncTask *stopBeep = [[ASyncTask alloc] init];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"2" forKey:@"id"];
    
    stopBeep.delegate = self;
    [stopBeep postRequestWithBaseURL:sensorTagBaseUrl urlPath:stopBeepUrl parameters:parameters];
}

- (IBAction)rightMenuSelected:(UIBarButtonItem *)sender
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.rootViewController presentRightMenuViewController];
}

#pragma mark AsyncTask Delegates

- (void)responseResult:(NSDictionary *)response JSON:(id)json type:(NSString *)type
{
    
}

- (void)responseFailure:(NSError *)error type:(NSString *)type
{
    
}

@end
