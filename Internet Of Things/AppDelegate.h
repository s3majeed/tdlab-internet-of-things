//
//  AppDelegate.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-08.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@protocol AppDelegate <NSObject>

@required

- (void)sensorLowBatteryWarning:(NSDictionary *)sensorData;
- (void)sensorExtremeTemperatureWarning:(NSDictionary *)sensorData;
- (void)sensorExtremeHumidityWarning:(NSDictionary *)sensorData;

@end


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIImage *defaultNavBackground;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) RootViewController *rootViewController;
@property (nonatomic) int navBarHeight;


@end

