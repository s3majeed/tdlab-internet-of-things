//
//  CreditHistorySelection.m
//  TDMobile
//
//  Created by Sulaiman Majeed on 2013-11-08.
//
//  Copyright (c) 2013 TDBFG. All rights reserved.
//

#import "AccountListViewController.h"
#import "UserData.h"

@interface AccountListViewController ()

@end

@implementation AccountListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    UserData *user = [UserData sharedInstance];
    int number = [user accountSections];
    
    return [user accountSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    UserData *user = [UserData sharedInstance];
    int number = [user accountsInSection:section];
    
    return [user accountsInSection:section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"Cell for row at index path");
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountCell" forIndexPath:indexPath];
    
    UILabel *accountName = (UILabel *)[cell viewWithTag:0];
    UILabel *accountNumber = (UILabel *)[cell viewWithTag:1];
    UILabel *amount = (UILabel *)[cell viewWithTag:2];
    
    // Configure the cell...
    UserData *user = [UserData sharedInstance];
    
    NSDictionary *item = [user accountAtIndex:indexPath.row section:indexPath.section];
    
    int x = 35;
    int humidityNumber = arc4random_uniform(100);
    int temperature = arc4random_uniform(100) % x;
    
    
    accountName.text = [item objectForKey:@"description"];
    accountNumber.text = [item objectForKey:@"acctNumber"];
    amount.text = [item objectForKey:@"balance"];
    
    //    NSArray *array = [[item objectForKey:@"title"] componentsSeparatedByString:@"- "];
    //
    //    //NSLog(@"Item: %@", item);
    //
    //    accountName.text = array[0];
    //    [accountName setFont:[UIFont boldSystemFontOfSize:15]];
    //    [accountName setTextColor:[UIColor blackColor]];
    //
    //    accountNumber.text = array[1];
    //    [accountNumber setFont:[UIFont boldSystemFontOfSize:13]];
    //    [accountNumber setTextColor:[UIColor grayColor]];
    //
    //    amount.text = [item objectForKey:@"balance"];
    //    [amount setFont:[UIFont boldSystemFontOfSize:15]];
    //    [amount setTextColor:[UIColor blackColor]];
    //    amount.textAlignment = NSTextAlignmentRight;
    
    NSLog(@"Accounts displayed");
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
