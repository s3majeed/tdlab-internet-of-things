//
//  LoginViewController.h
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-08.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASyncTask.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate, ASyncTaskDelegate>
{
    
}

- (IBAction)loginClicked:(id)sender;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UISwitch *remember;

@end
