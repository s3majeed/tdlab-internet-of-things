
//
//  LoginViewController.m
//  Internet Of Things
//
//  Created by Sulaiman on 2015-04-08.
//  Copyright (c) 2015 TD Bank. All rights reserved.
//

#import "LoginViewController.h"
#import "AccountListViewController.h"
#import "UserData.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "WirelessConnection.h"
#import "DataParser.h"
#import "ASyncTask.h"

@interface LoginViewController ()

@property CGPoint originalViewCenter;

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [self.remember setOnTintColor:[UIColor colorWithRed:58.0/255.0 green:133.0/255.0 blue:73.0/255.0 alpha:1]];
    
    NSString *accessCard = [[NSUserDefaults standardUserDefaults] stringForKey:@"accessCard"];
    
    NSLog(@"Saved Card:%@", accessCard);
    
    if (![accessCard  isEqual: @""])
    {
        NSMutableString *maskedAccessCard = [accessCard mutableCopy];
        
        if ([maskedAccessCard length] < 12)
        {
            [maskedAccessCard replaceCharactersInRange:NSMakeRange(4, 4) withString:@"****"];
        }
        else
        {
            [maskedAccessCard replaceCharactersInRange:NSMakeRange(4, 8) withString:@"********"];
        }
        
        self.username.text = maskedAccessCard;
    }
    
    UserData *user = [UserData sharedInstance];
    [user setLoggedIn:NO];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (cookie in [storage cookies])
    {
        // NSLog(@"Cookie: %@", cookie);
        [storage deleteCookie:cookie];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    self.username.delegate = self;
    self.password.delegate = self;
    
    [self.activityIndicator setColor:[UIColor greenColor]];
    
    self.activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                               UIViewAutoresizingFlexibleRightMargin |
                                               UIViewAutoresizingFlexibleTopMargin |
                                               UIViewAutoresizingFlexibleBottomMargin);
    
    self.activityIndicator.center = CGPointMake(CGRectGetWidth(self.view.bounds)/2, CGRectGetHeight(self.view.bounds)/2);
    
    NSLog(@"Point: %f, %f", self.view.bounds.size.width/2.0, self.view.bounds.size.height/2.0);
    
    [self.view addSubview:self.activityIndicator];
    
    NSMutableDictionary *humidityDictionary = [[NSMutableDictionary alloc] init];
    [humidityDictionary setObject:@"0f32cb87-eb86-4a60-9e66-e0cfd661cba1" forKey:@"uuid"];
    [humidityDictionary setObject:@"2014-10-12" forKey:@"fromDate"];
    [humidityDictionary setObject:@"2014-10-12" forKey:@"toDate"];
    
    ASyncTask *humidityAsyncTask = [[ASyncTask alloc] init];
    humidityAsyncTask.type = @"humiditySensor";
    humidityAsyncTask.delegate = self;
    
    [humidityAsyncTask postRequestWithBaseURL:@"https://www.mytaglist.com/" urlPath:@"https://www.mytaglist.com/ethLogShared.asmx/GetStatsRawByUUID" parameters:humidityDictionary];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginClicked:(id)sender
{
    if ([self.username.text length] == 0 || [self.password.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your username and password" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
        [alert show];
    }
    else
    {
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];

        [parameters setObject:@"LI" forKey:@"ID"];
        
        NSString *username = self.username.text;
        
        if ([username rangeOfString:@"*"].location == NSNotFound)
        {
            if (self.remember.on)
            {
                NSString *valueToSave = username;
                [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"accessCard"];
            }
            else
            {
                NSString *valueToSave = @"";
                [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"accessCard"];
            }
        }
        else
        {
            username = [[NSUserDefaults standardUserDefaults] stringForKey:@"accessCard"];
            
            if (!self.remember.on)
            {
                NSString *valueToSave = @"";
                [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"accessCard"];
            }
        }
        
        [parameters setObject:username forKey:@"MASKEDUID"];
        [parameters setObject:self.password.text forKey:@"PSWD"];
        
        
        UserData *user = [UserData sharedInstance];
        [user deleteUserCookies];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.activityIndicator startAnimating];
        [self.view setUserInteractionEnabled:NO];
        
        WirelessConnection *connection = [WirelessConnection sharedInstance];
        ASyncTask *authenticationAsyncTask = [[ASyncTask alloc] init];
        authenticationAsyncTask.delegate = self;
        [authenticationAsyncTask getWithURLRequest:[connection getAuthenticationRequest:parameters] type:@"authentication"];
    }
}

#pragma mark Textfield Methods

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _password)
    {
        self.view.center = self.originalViewCenter;
        [textField resignFirstResponder];
        [self loginClicked:nil];
        return NO;
    }
    else
    {
        self.view.center = self.originalViewCenter;
        [self.password becomeFirstResponder];
        return YES;
    }
    
    return YES;
}


#pragma mark AsyncTask Delegates

- (void)responseResult:(NSDictionary *)response JSON:(id)json type:(NSString *)type
{
    UserData *userData = [UserData sharedInstance];
    
    if ([type  isEqual: @"humiditySensor"])
    {
        [userData setSensorData:response];
    }
    
    if ([type  isEqual: @"authentication"])
    {
        [self.activityIndicator stopAnimating];
        [self.view setUserInteractionEnabled:YES];
        
        DataParser *dataParser = [DataParser sharedInstance];
        
        if ([dataParser parseLoginResponse:json])
        {
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            [parameters setObject:@"INT" forKey:@"ID"];          // Accounts
            
            [self.activityIndicator startAnimating];

            WirelessConnection *connection = [WirelessConnection sharedInstance];
            
            ASyncTask *accountAsyncTask = [[ASyncTask alloc] init];
            accountAsyncTask.delegate = self;
            [accountAsyncTask getWithURLRequest:[connection getBankingRequest:parameters] type:@"accounts"];
        }
    }
    
    if ([type  isEqual: @"accounts"])
    {
        [self.activityIndicator stopAnimating];
        
        DataParser *dataParser = [DataParser sharedInstance];
        NSMutableDictionary *accounts = [dataParser parseAccountsResponse:json];
            
        AccountListViewController *accountList = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountList"];
        [self.navigationController pushViewController:accountList animated:YES];
    }
}

- (void)responseFailure:(NSError *)error type:(NSString *)type
{
    if ([type  isEqual: @"humiditySensor"])
    {
        NSLog(@"Error: %@", error);
    }
    
    if ([type  isEqual: @"authentication"])
    {
        NSLog(@"Error: %@", error);
    }
    
    if ([type  isEqual: @"accounts"])
    {
        NSLog(@"Error: %@", error);
    }
    
    [self.activityIndicator stopAnimating];
    [self.view setUserInteractionEnabled:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
